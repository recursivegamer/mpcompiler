#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <queue>
#include <stack>
#include <string.h>
using namespace std;

struct symbolTable
{
	string lexeme;
	string kind;
	string type;
	int offSet;
	symbolTable *nextSymbol;
	symbolTable *nextTable;
};

struct tokenLexeme
{
	string token;
	string lexeme;
	int row;
	int column;
};

class Analyzer
{
public:
	queue<tokenLexeme> tmPair;
	Analyzer(void);
	symbolTable *analysisRoot;
	stack<string> opStack;
	ofstream codeFile;
	int regNum, labelCounter;
	string forInc;
	string relOpType;
	bool err;
	bool hasFloat;
	bool flag;
	bool getErr(void);
	int genLabel(void);
	int genLabel(int exitLabel);
	int loopBranch(int loopLabel); 
	int repUnLoopBranch(int loopLabel);
	void genEndLoop(int loopLabel);
	int loopCheckFirstBranch(int loopLabel);
	int ifBranch();
	int branchAfterIfTrue(void);
	void setForInc(string forInc); 
	void setRegNum(int newNum);
	void pushControl(string incRec);
	void genForInc(string reg);
	void assignTable(symbolTable *currentTable);
	void genAssign(string id_rec, queue<tokenLexeme> exp_rec);
	void handleNextToNegate(string type, tokenLexeme next);
	bool isBoolExp(queue<tokenLexeme> exp_rec);
	void genExp(string type, queue<tokenLexeme> exp_rec);
	void completeSubExpForBool(string type);
	void genMul(string type, string op);
	void genAdd(string type, string op);
	void genBool(string op);
	bool isAddOp(string opCheck);
	bool isMulOp(string opCheck);
	bool isRelOp(string relCheck);
	bool isBoolOp(string boolCheck);
	void genCastToFloat(void);
	void genCastToInt(void);
	void genNeg(string type);
	void genVarLit(string type, tokenLexeme varLit);
	bool checkForFloat(queue<tokenLexeme> exp_rec);
	void genRead(string rdId);
	void genWrt(void);
	void genWrtLn(void);
	void genLoop(string register1, string register2, string type, string op);
	void genForCmp(string type);
	void genRelOp(string type, string op);
	void finishOps(string type, bool optionalSign);
	void initVars(int numVars, int regNum);
	void genHalt(void);
	symbolTable *findInSymbolTable(string lookingFor);
	~Analyzer(void);
};
