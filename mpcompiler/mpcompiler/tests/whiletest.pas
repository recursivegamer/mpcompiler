program whileTest;

var a, b: integer;

begin

read(a, b);

	writeln(a);

	while a > b do
	begin
	  a := a - 1;
	  writeln(a);
	end;

	a := 20;
	writeln(a);

	while a >= b do
	begin
	  a := a - 1;
	  writeln(a);
	end;

	a:= 0;
	writeln(a);

	while a < b do
	begin
	  a := a + 1;
	  writeln(a);
	end;

	a := 0;
	writeln(a);

	while a <= b do
	begin
	  a := a + 1;
	  writeln(a);
	end;


end.
