{repeat test}
program repeatTest;

var a, b: integer;

begin

a := 1;
b := 5;
repeat
 writeln(a, b);
	a := a + 1;
until a > b;

end.
