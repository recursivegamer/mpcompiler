program Tester;

	var I: Integer;
	
		begin {tester}
			Write('Please enter an integer value for I: ');
			Read(I);
			I := I + 1;
			Write('The current value of I is ', I);
		end. {tester}
