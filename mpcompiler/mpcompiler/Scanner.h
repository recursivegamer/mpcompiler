#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "string.h"



using namespace std;

struct errorInfo 
{
        string errToken;
        string errLexeme;
        string errString;
        int errRow;
        int errColumn;
};


class Scanner
{
public:
	Scanner(void);
	
	ofstream outFile;
	int lineCount;
	int columnCount;
	errorInfo errs[100];
	int errCount;
	void openFile(string fileName);
	void getToken(string data);
	void identifierToken(string currentToken);
	void numbersToken(string currentToken);
	void stringLiteralToken(string currentToken);
	string reservedWordCheck(string lookUpString);
	~Scanner(void);
};

